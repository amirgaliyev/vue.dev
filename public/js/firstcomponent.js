Vue.component('message',{
    props: ['header','body'],
    template: `
        <div>
            <article class="message" v-show="isVisible">
                <div class="message-header">
                    <p>{{header}}</p>
                    <button class="delete" aria-label="delete" v-on:click="hideMessage"></button>
                </div>
                <div class="message-body">
                {{body}}
                </div>
            </article>
        </div>
    `,
    data(){
        return {
            isVisible: true
        }
    },
    methods: {
        hideMessage(){
            return this.isVisible = false;
        }

    }
});

new Vue({
    el:'#app',
});