Vue.component('task-list',{
    template:`
    <div>
        <task v-for="task in tasks">{{task.task}}</task>
        </div>
    `,
    data(){
        return {
            tasks:[
                {task: 'Go to the school'},
                {task: 'Go to the fitnes'},
                {task: 'Go to the work'},
                {task: 'Go to the university'}
                ]
        };
    }
});

Vue.component('task',{
    template: '<li><slot></slot></li>'
});

