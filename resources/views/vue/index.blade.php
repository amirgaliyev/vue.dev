<html>
<head>
    <title>Vue</title>

    <style>
        .is-loading{
            background-color: red;
        }
    </style>
</head>

<body>

    <div id="root">
        <input type="text" id="input" v-model="message">
        <p>The value of the message is: @{{message}}</p>

        <ul>
            <li  v-for="name in names">
                @{{ name }}
            </li>
        </ul>

        <input  type="text" v-model="newName">
        <button v-on:click="addName">Add names</button>

        <button v-bind:class="{'is-loading' : isLoading}" v-on:click="chooseColor">Chose the color</button>
        <button v-bind:disabled="isDisabled">Disabled</button>


        <h1>All Tasks</h1>
        <ul>
            <li v-for="task in tasks">@{{task.description}}</li>
        </ul>

        <h1>Incompleted Tasks</h1>
        <ul>
            <li v-for="task in incompletedTasks">@{{task.description}}</li>
        </ul>

        <task-list></task-list>

    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="/js/main.js"></script>

    <script>

       var app = new Vue({
            el: '#root',
            data: {
                message: 'Hello world',
                newName: '',
                names: ['Almas','Gulasyl','Talshyn','Sholpan','Sovet'],

                isLoading: false,
                isDisabled: false,

                tasks:[
                    {description:'learn Laravel', completed: false},
                    {description:'learn Vue', completed: true },
                    {description:'learn Ajax', completed: false},
                    {description:'learn Angular', completed: false},
                    {description:'learn Java', completed: true},
                    {description:'learn Spring', completed: false},
                ]
            },
           computed:{
             incompletedTasks(){
                 return this.tasks.filter(task => !task.completed);
             }
           },
           methods:{
                addName(){
                    this.names.push(this.newName);
                    this.newName = '';
                },
               chooseColor(){
                    if(this.isLoading == false) {
                        this.isLoading = true;
                        this.isDisabled = true;
                    }else{
                        this.isLoading=false;
                        this.isDisabled = false;
                    }
               },
           }

            /*
            mounted(){
                document.querySelector('#button').addEventListener('click', () => {
                    let name = document.querySelector('#inputName');

                app.names.push(name.value);

                name.value ='';
            });
            }
            */
        });


    </script>

</body>
</html>